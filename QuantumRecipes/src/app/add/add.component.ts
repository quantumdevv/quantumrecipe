import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgControlStatus, FormArray } from '@angular/forms';
import { stringify } from 'querystring';
import { IRecipe } from '../recipe';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';
import { IIngredient } from '../ingredient';
import { DataService } from '../data.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  recipe: IRecipe = <IRecipe>{};
  ingredientArray: IIngredient[];
  addForm: FormGroup;
  ingredientForm: FormGroup;
  submitted = false;
  success = false;
  count = 0;
  
  constructor(private formBuilder: FormBuilder,private data: DataService) { 
    this.addForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      instructions: ['', Validators.required],
      ingredients:  this.formBuilder.array([
        this.addIngredientFormGroup()
      ])
    });  
  }

  
  ngOnInit() {
  }

  onSubmit(){
    this.submitted = true;
    if(this.addForm.invalid){
      return;
    }

    //get recipe information and set it to object recipe
     this.recipe.title = this.addForm.get('title').value;
     this.recipe.description = this.addForm.get('description').value;
     this.recipe.instructions = this.addForm.get('instructions').value;
     this.recipe.ingredients = this.addForm.get('ingredients').value;
     //adds recipe
     this.data.addRecipe(this.recipe);
     alert('Your recipe has been added!');
     //redirects to home page
     window.location.replace("/app/");
  }

  newIngredient(){
    //creates a new ingredient (the controls for a new ingredient)
   (<FormArray>this.addForm.get('ingredients')).push(this.addIngredientFormGroup());
  }

addIngredientFormGroup(): FormGroup{
    //adds ingredient form group
  this.count++;
  return this.formBuilder.group({
    name: ['',Validators.required],
    amount: ['',Validators.required]
  });
}

  removeIngredient(){
      //removes ingredient form group
    var ar = (<FormArray>this.addForm.get('ingredients'));
    if (ar.length > 1){
      ar.removeAt(ar.length - 1);
    }
  }
}
