import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IRecipe } from './recipe';
import { Observable } from 'rxjs';
import {HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getRecipes() : Observable<IRecipe[]>{
    return this.http.get<IRecipe[]>('http://54.196.44.12:80/api/recipe/getrecipes/');
  }

  getRecipe(id: number): Observable<IRecipe>{
    return this.http.get<IRecipe>('http://54.196.44.12:80/api/recipe/getrecipe?id=' + id);
  }

  addRecipe(recipe: IRecipe){
    let headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    var options = {
      headers: headers
    };
    this.http.post('http://54.196.44.12:80/api/recipe/addrecipe/', recipe, options).subscribe();
  }

  deleteRecipe(id: number){ 
    return this.http.delete('http://54.196.44.12:80/api/recipe/deleterecipe?id=' + id);
  }

  updateRecipe(recipe: IRecipe){
    let headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    });
    var options = {
      headers: headers
    };
    this.http.put('http://54.196.44.12:80/api/recipe/updaterecipe/', recipe, options).subscribe();
  }
}
