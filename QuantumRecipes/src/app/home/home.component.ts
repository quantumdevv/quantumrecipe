import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { IRecipe } from '../recipe';
import { IIngredient } from '../ingredient';
import { timer } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(private data: DataService) { }

  public recipes: IRecipe[];

  ngOnInit() {
    //gets all recipes
    this.data.getRecipes().subscribe(data =>{
      //saves the data into object recipes
      this.recipes = data
    });
  }

  showDetails(index: number){
    //gets recipe details for clicked recipe
    var details = document.getElementById("recipeDetails"+index);
    var buttons = document.getElementById("recipeButtons"+index);
    //checks current show/hide state
    if(details.style.display == "none"){
      //shows recipe details
      details.style.display = "block";
      //shows recipe buttons
      buttons.style.display = "block";
    }else{
       //hides recipe details
      details.style.display = "none";
      //hides recipe buttons
      buttons.style.display = "none";
    }
  }
  deleteRecipe(index: number){
    //gets the clicked recipe id
    var recipeId = parseInt(document.getElementById("recipeId"+index).innerHTML);
    //confirms user wants to delete recipe
    if(confirm('Are you sure you want to delete this recipe?')){
      //calls delete api call
      this.data.deleteRecipe(recipeId).subscribe( 
        (data) =>{
          //refreshes page
          this.ngOnInit();
        }),
        err => {
          console.log("Error");
        }; 
    }
  }

  editRecipe(index: number){
    //gets the clicked recipe id
    var recipeId = parseInt(document.getElementById("recipeId"+index).innerHTML);
    //redirects to update with the recipe id
    window.location.href = "/app/update/"+recipeId.toString();
  }
}
