export interface IIngredient{
    id: number,
    recipeId: number,
    name: string,
    amount: string
}