import { IIngredient } from './ingredient';

export interface IRecipe{
    id: number,
    title: string,
    description: string,
    instructions: string,
    ingredients: IIngredient[]
}