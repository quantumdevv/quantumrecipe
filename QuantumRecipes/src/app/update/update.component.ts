import { Component, OnInit } from '@angular/core';
import { IRecipe } from '../recipe';
import { DataService } from '../data.service';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { AppModule } from '../app.module';
import { ActivatedRoute } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { IIngredient } from '../ingredient';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  id: number;
  recipe: IRecipe = <IRecipe>{};
  ingredientsArray: IIngredient[] = <IIngredient[]>{};
  ingredientForm: FormGroup;
  updateForm: FormGroup;
  submitted = false;
  success = false;
  count = 0;

  constructor(private formBuilder: FormBuilder, private data: DataService, route: ActivatedRoute) {
   
    this.updateForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      instructions: ['', Validators.required],
      ingredients: this.formBuilder.array([])
    });
    //grabs the route paramater and sets it to id
    route.params.subscribe( params => this.id = params['id']);
  }

  ngOnInit() {
     //gets all recipes
    this.data.getRecipe(this.id).subscribe(data =>{
      //saves the data into object recipes
      this.recipe = data
      this.ingredientsArray = this.recipe.ingredients
      
      this.updateForm.get('title').setValue(this.recipe.title);
      this.updateForm.get('description').setValue(this.recipe.description);
      this.updateForm.get('instructions').setValue(this.recipe.instructions);
      var ingredientfrm = (<FormArray>this.updateForm.get('ingredients'));
      this.ingredientsArray.forEach(i=>{
        var ing = this.formBuilder.group({
          name: [i.name,Validators.required],
          amount: [i.amount,Validators.required],
          id: [i.id]
        });
        //pust form builder group to form builder array
        ingredientfrm.push(ing);
      });
    });
  }
   

  addIngredientFormGroup(): FormGroup{
    //adds ingredient form group
    this.count++;
    return this.formBuilder.group({
      name: ['',Validators.required],
      amount: ['',Validators.required],
      id: [0]
    });
  }
  
  newIngredient(){
    if(this.updateForm.invalid){
      return;
    }
    //add ingredient buttons
    (<FormArray>this.updateForm.get('ingredients')).push(this.addIngredientFormGroup());
   }

   removeIngredient(){
     //remove ingredient buttons
    var ar = (<FormArray>this.updateForm.get('ingredients'));
    if (ar.length > 1){
      ar.removeAt(ar.length - 1);
    }
  }

  onSubmit(){
    this.submitted = true;
    if(this.updateForm.invalid){
      return;
    }
    //gets the updated recipe information
    //and saves it to object recipe
    this.recipe.id = this.id;
    this.recipe.title = this.updateForm.get('title').value;
    this.recipe.description = this.updateForm.get('description').value;
    this.recipe.instructions = this.updateForm.get('instructions').value;
    this.recipe.ingredients = this.updateForm.get('ingredients').value;
    //updates recipe
    this.data.updateRecipe(this.recipe);
    alert('Your recipe has been Updated!');
    //redirects to home page
    window.location.replace("/app/");
  }
}
